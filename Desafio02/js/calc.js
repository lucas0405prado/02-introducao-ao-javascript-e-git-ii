let botao0 = document.getElementById('0');
let botao1 = document.getElementById('1');
let botao2 = document.getElementById('2');
let botao3 = document.getElementById('3');
let botao4 = document.getElementById('4');
let botao5 = document.getElementById('5');
let botao6 = document.getElementById('6');
let botao7 = document.getElementById('7');
let botao8 = document.getElementById('8');
let botao9 = document.getElementById('9');
let botaoAdicao = document.getElementById('+');
let botaoSubtracao = document.getElementById('-');
let botaoMultiplicao = document.getElementById('x');
let botaoDivisao = document.getElementById('/');
let botaoClear = document.getElementById('clear');
let botaoIgual = document.getElementById('igual');
let textTela = document.getElementById('text-tela');

let conta = '';
let valoresDaOperacao
let ultimoDigitoFoiOperador = false
let existeOperadorEspecial = true



function verificarTamanhoDaOperacao() {
    if(conta.length > 22) {
        textTela.textContent = 'Operação Grande Demais'
        setTimeout(function(){
            textTela.textContent = '0'; 
            conta = '';
        },1000)
    }
}

function buttonClick(event) {
    let idEvent = event.path[0].id;
    toString(idEvent)

   

    if (idEvent == "+" || idEvent == "-" || idEvent == "/" || idEvent == "x"   ) {

        idEvent = verificarOperador(idEvent)

        
        if (textTela.textContent == 0 && idEvent != "-" ) {
            conta = ""
            idEvent = ""
        } else if (idEvent == "-") {
            idEvent = "-"
        }

    } else {
        ultimoDigitoFoiOperador = false
    }

    
    conta +=  idEvent;


    textTela.textContent = conta
    verificarTamanhoDaOperacao()
}

let verificarOperador = (idEvent) => {
    let valorDeRetorno
    let operacao = textTela.textContent
    let textOperacao = operacao.split(" ")
    let ultimoIndiceDaOperacao = textOperacao.length - 1
    let numbersRejex = /[0-9]/g

    if (ultimoDigitoFoiOperador) {
        valorDeRetorno = ""

        if ( numbersRejex.test(textOperacao[ultimoIndiceDaOperacao])) {
            ultimoDigitoFoiOperador = false
        }
        
    } else {
        switch (idEvent) {
            case "+" : valorDeRetorno = " + "
            break
    
            case "-" : valorDeRetorno = " - "
            break
    
            case "x" : valorDeRetorno = " x "
            break
    
            case "/" : valorDeRetorno = " / "
            break
        }
        
        ultimoDigitoFoiOperador = true
    }

   
   
    
    if (idEvent == "-" && conta == "" && textTela.textContent == "0" ) {

        conta = ""
        valorDeRetorno = "-"

    }  else if (idEvent == "-" && conta == "") {
        valorDeRetorno = "-"
        primeiroDigito = false
    } 

    return valorDeRetorno
}

let calcularExpressao = (valoresDaOperacao) => {

    let operacao
    let valor1 = parseFloat(valoresDaOperacao[0])
    let valor2 =  parseFloat(valoresDaOperacao[2])

    switch (valoresDaOperacao[1]){
        case "+" : operacao = valor1 + valor2
        break

        case "-": operacao = valor1 - valor2
        break

        case "x" : operacao = valor1 * valor2
        break

        case "/": operacao = valor1 / valor2
        break
    }

    return operacao
 
}

let calcularOperacao = () => {
    let operacao = textTela.textContent
    valoresDaOperacao = operacao.split(" ")
   


    if (valoresDaOperacao.length < 3 || (valoresDaOperacao[2] == "" )) {
        textTela.textContent = "Faltam dados"

        setTimeout(function(){
            textTela.textContent = '0'; 
            conta = '';
        },1000)
   



    } else if (valoresDaOperacao.length == 3) {
        let expressaoCalculada = calcularExpressao(valoresDaOperacao)
        mostrarResultado(expressaoCalculada)

    } else if (valoresDaOperacao.length > 3) {
       fatorarExpressao()
    }

   
}

let mostrarResultado = (operacao) => {
    textTela.textContent =  operacao
    conta = operacao
}

let fatorarExpressao = () => {
    let operacao = textTela.textContent
    let resultado 
    existeOperadorEspecial = true

    for ( let index = 0; existeOperadorEspecial == true; index++) {
        resultado = calcularOperacaoComOperadoresEspeciais()
        
    }

    if ( valoresDaOperacao.length == 1 ) {
        conta = resultado
        textTela.innerText = resultado
    } else {

        let expressaoCalculada

        
        expressaoCalculada = calcularOperacaoSemOperadoresEspeciais()
       
        mostrarResultado(expressaoCalculada)
    }
   
}

let calcularOperacaoSemOperadoresEspeciais = () => {
 
   let resultado

    for (let index = 0; valoresDaOperacao.length != 1; index++) {
        let valor1 = parseFloat(valoresDaOperacao[0])
        let valor2 = parseFloat(valoresDaOperacao[2])

        switch (valoresDaOperacao[1]) {
            case "+" : resultado = valor1 + valor2;
            break

            case "-" : resultado = valor1 - valor2
            break;
        }

            
            valoresDaOperacao[0] = resultado
            valoresDaOperacao.splice(1,2)

    }

    return valoresDaOperacao[0]
}

let calcularOperacaoComOperadoresEspeciais = () => {

    let indexOperadorEspecial = encontarOperadorEspecial()

    let operacao = valoresDaOperacao[indexOperadorEspecial]
    
    let valorPrimarioDaOperacao = parseInt(valoresDaOperacao[indexOperadorEspecial - 1]) 
    let valorSecundarioDaOperacao = parseInt(valoresDaOperacao[indexOperadorEspecial + 1])
    let resultadoOperacao 

    switch (operacao) {
        case "x" : resultadoOperacao = valorPrimarioDaOperacao * valorSecundarioDaOperacao
        break

        case "/" : resultadoOperacao = valorPrimarioDaOperacao / valorSecundarioDaOperacao
    }


    if (indexOperadorEspecial != -1) {
        valoresDaOperacao[indexOperadorEspecial - 1] = resultadoOperacao
         valoresDaOperacao.splice(indexOperadorEspecial,2)

         return valoresDaOperacao[valoresDaOperacao.length - 1]
    } else {
        existeOperadorEspecial = false
        return valoresDaOperacao[valoresDaOperacao.length - 1]
    }

    
}

let encontarOperadorEspecial = () => {
    
    let possueOperadorMultiplicar = false 
    let possueOperadorDividir = false
    let primeiroOperadorEspecial

    if (valoresDaOperacao.indexOf("x") != -1 ) {
        possueOperadorMultiplicar = true
    }

    if (valoresDaOperacao.indexOf("/") != -1) {
        possueOperadorDividir = true
      
    }


    if (possueOperadorMultiplicar  && possueOperadorDividir) {

        primeiroOperadorEspecial = valoresDaOperacao.indexOf("x") < valoresDaOperacao.indexOf("/") ? valoresDaOperacao.indexOf("x") : valoresDaOperacao.indexOf("/")
    }

    if (possueOperadorMultiplicar && possueOperadorDividir == false) {
        primeiroOperadorEspecial = valoresDaOperacao.indexOf("x")
    }

    if (possueOperadorMultiplicar == false && possueOperadorDividir) {
        primeiroOperadorEspecial = valoresDaOperacao.indexOf("/")
    }

    if (possueOperadorMultiplicar == false && possueOperadorDividir == false) {
        primeiroOperadorEspecial = -1
    }

    return primeiroOperadorEspecial
}

let apagarValores = () => {
    conta = ""
    valoresDaOperacao = []
    textTela.textContent = "0"
}


botaoIgual.addEventListener('click', calcularOperacao)
botao0.addEventListener('click',buttonClick) 
botao1.addEventListener('click',buttonClick) 
botao2.addEventListener('click',buttonClick) 
botao3.addEventListener('click',buttonClick) 
botao4.addEventListener('click',buttonClick) 
botao5.addEventListener('click',buttonClick) 
botao6.addEventListener('click',buttonClick) 
botao7.addEventListener('click',buttonClick) 
botao8.addEventListener('click',buttonClick) 
botao9.addEventListener('click',buttonClick) 
botaoAdicao.addEventListener('click',buttonClick) 
botaoSubtracao.addEventListener('click',buttonClick) 
botaoMultiplicao.addEventListener('click',buttonClick) 
botaoDivisao.addEventListener('click',buttonClick) 
botaoClear.addEventListener('click',apagarValores)